
public class Diff implements Visitor {

	@Override
	public Operation visit(PlusOperation n) {
		
		return new PlusOperation((Operation)(n.getLeft().accept(this)), (Operation)(n.getRight().accept(this)));
		
	}

	@Override
	public Operation visit(MinusOperation n) {
		
		return new MinusOperation((Operation)(n.getLeft().accept(this)), (Operation)(n.getRight().accept(this)));
	}

	@Override
	public Operation visit(MultiplyOperation n) {
		
		return new PlusOperation(new MultiplyOperation((Operation)(n.getLeft().accept(this)), (Operation)(n.getRight())), new MultiplyOperation((Operation)(n.getLeft()), (Operation)(n.getRight().accept(this))));
		
	}

	@Override
	public Operation visit(DivideOperation n) {
		
		return new DivideOperation(new MinusOperation(new MultiplyOperation((Operation)(n.getLeft().accept(this)), (Operation)(n.getRight())), new MultiplyOperation((Operation)(n.getLeft()), (Operation)(n.getRight().accept(this)))), new MultiplyOperation((Operation)(n.getRight()), (Operation)(n.getRight())));
		
	}
	
	@Override
	public Operation visit(Variable n) {
		
		return new Number('1');
	}

	@Override
	public Operation visit(Number n) {
		
		return new Number('0');
	}
}
