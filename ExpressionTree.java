
public class ExpressionTree {

	/** Stack which used to store TreeCell (non-OOP) for makeExpTree method. */
	private static Stack <TreeCell<Character>> stack = new ListStack <TreeCell<Character>> ();

	/** Stack which used to store TreeCell (OOP) for makeExpTree method. */
	private static Stack<Operation> stackOOP = new ListStack<Operation>();

	/** Stack which used to store character for intoPos method. */
	private static Stack <Character> operators = new ListStack <Character> ();

	/**
	 * Convert infix-expression to postfix-expression.
	 * @param infix as an input
	 * @return converted postfix-expression
	 */
	public static String intoPos(String infix) {

		// Base case : end of input

		if(infix.length() == 0) {

			String temp = "";

			// if stack is not empty, then pop until it's empty.

			while(!operators.isEmpty()) {

				temp += operators.pop();

			}

			return temp;

		}

		// if character is digit, then output it.

		if(Character.isDigit(infix.charAt(0))) {

			return infix.charAt(0) + intoPos(infix.substring(1));

		}

		if(infix.charAt(0) == 'x') {

			return infix.charAt(0) + intoPos(infix.substring(1));

		}

		// if character is open-parenthesis, then push it into stack.

		else if(infix.charAt(0) == '(') {

			operators.push('(');

			return intoPos(infix.substring(1));
		}

		// if character is close-parenthesis, then pop until open-parenthesis is detected.

		else if(infix.charAt(0) == ')') {

			String temp = "";

			while(!operators.isEmpty() && operators.peek() != '(') {

				temp += operators.pop();

			}

			operators.pop();

			return temp + intoPos(infix.substring(1));

		}

		// if character is multiply or divide and top of stack (TOS) is multiply or divide, then pop into output and push current into stack.

		else if((infix.charAt(0) == '*' || infix.charAt(0) == '/') && !operators.isEmpty() && (operators.peek() == '*' || operators.peek() == '/')) {

			char temp = operators.pop();

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));
		}

		// if character is multiply or divide and top of stack (TOS) is not multiply or divide, then push current into stack.

		else if(infix.charAt(0) == '*' || infix.charAt(0) == '/') {

			operators.push(infix.charAt(0));

			return intoPos(infix.substring(1));

		}

		// if character is add or minus and top of stack (TOS) is multiply or divide, then pop until open-parenthesis is detected and push current into stack.

		else if((infix.charAt(0) == '+' || infix.charAt(0) == '-') && !operators.isEmpty() && (operators.peek() == '*' || operators.peek() == '/')) {

			String temp = "";

			while(!operators.isEmpty() && operators.peek() != '(') {

				temp += operators.pop();

			}

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));

		}

		// if character is add or minus and top of stack (TOS) is add or minus, then pop into output and push current into stack.

		else if((infix.charAt(0) == '+' || infix.charAt(0) == '-') && !operators.isEmpty() && (operators.peek() == '-' || operators.peek() == '+')) {

			char temp = operators.pop();

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));
		}

		// if character is add or minus and top of stack (TOS) is not add or minus, then push current into stack.

		else if(infix.charAt(0) == '+' || infix.charAt(0) == '-') {

			operators.push(infix.charAt(0));

			return intoPos(infix.substring(1));
		}

		else {

			return intoPos(infix.substring(1));
		}

	}

	/* This method returns a root to the equivalent expression tree of the input postfix form. */

	public static TreeCell<Character> makeExpTree(String postfix) {

		if(postfix.length() == 0) return stack.pop(); // Base case : end of input

		TreeCell<Character> cell = new TreeCell<Character>(postfix.charAt(0));

		// if cell kind is operator, pop stack and set right then pop stack again and set left.

		// Otherwise, do nothing.

		if(cell.getKind() == Kind.OPERATOR) {

			cell.setRight(stack.pop());

			cell.setLeft(stack.pop());

		}

		// Push it into stack.

		stack.push(cell);

		return makeExpTree(postfix.substring(1));

	}

	/* This method returns a root to the equivalent expression tree of the input postfix form (OOP). */

	public static Operation makeExpTreeOOP(String postfix) {

		if(postfix.length() == 0) return stackOOP.pop(); // Base case : end of input

		Operation cell;

		// Create operation depending on operators.

		switch(postfix.charAt(0)) {

		case '+' :

			cell = new PlusOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '-' :

			cell = new MinusOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '*' :

			cell = new MultiplyOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '/' :

			cell = new DivideOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case 'x' :

			cell = new Variable(postfix.charAt(0));

			break;

		default :

			cell = new Number(postfix.charAt(0));

			break;

		}

		stackOOP.push(cell);

		return makeExpTreeOOP(postfix.substring(1));

	}

	// Print prefix-expression from TreeCell.

	public static void printPrefix(TreeCell<?> root) {

		System.out.print(root.getDatum());

		if(root.isLeaf()) return;

		printPrefix(root.getLeft());

		printPrefix(root.getRight());

	}

	// Print infix-expression from TreeCell.

	public static void printInfix(TreeCell<Character> root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;
		}

		printInfix(root.getLeft());

		System.out.print(root.getDatum());

		printInfix(root.getRight());

	}

	// Print postfix-expression from TreeCell.

	public static void printPostfix(TreeCell<?> root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;

		}

		printPostfix(root.getLeft());

		printPostfix(root.getRight());

		System.out.print(root.getDatum());

	}

	// Print prefix-expression from Operation (OOP).

	public static void printPrefixOOP(Operation root) {

		System.out.print(root.getDatum());

		if(root.isLeaf()) return;

		printPrefixOOP(root.getLeft());

		printPrefixOOP(root.getRight());

	}

	// Print infix-expression from Operation (OOP).

	public static void printInfixOOP(Operation root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;
		}
		
		System.out.print("(");

		printInfixOOP(root.getLeft());

		System.out.print(root.getDatum());

		printInfixOOP(root.getRight());
		
		System.out.print(")");

	}

	// Print postfix-expression from Operation (OOP).

	public static void printPostfixOOP(Operation root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;

		}

		printPostfixOOP(root.getLeft());

		printPostfixOOP(root.getRight());

		System.out.print(root.getDatum());

	}

	public static void printTree(TreeCell<Character> root, int level ) {

		if (root != null) {

			printTree(root.getRight(), level+1);

			for (int i = 0; i < level; i++) System.out.print(" ");

			System.out.println(root.getDatum());

			System.out.println();

			printTree(root.getLeft(), level+1);

		}
	}

	// Evaluate the expression tree.

	public static int eval(TreeCell<Character> root) {

		if(root.isLeaf()) return root.getDatum() - '0';

		switch(root.getDatum()) {

		case '+' :

			return eval(root.getLeft()) + eval(root.getRight());

		case '-' :

			return eval(root.getLeft()) - eval(root.getRight());

		case '*' :

			return eval(root.getLeft()) * eval(root.getRight());

		case '/' :

			return eval(root.getLeft()) / eval(root.getRight());

		}

		return root.getDatum();
	}

	public static void printTree(Operation root, int level) {

		if(root != null) {

			printTree(root.getRight(), level + 1);

			for(int i = 0; i < level; i++) System.out.print(" ");

			System.out.println(root.getDatum());

			System.out.println();

			printTree(root.getLeft(), level + 1);
		}
	}

	public static void main(String[] args) {

		String expression1 = "4 + 5 * 6 + ( 1 + 3 ) / 2";

		String expression2 = "2 * 3 / ( 2 - 1 ) + 6 * ( 5 + 4 )";

		String expression3 = "2 * ( ( 8 / 2 ) / 4 ) - ( 9 - 5 )";

		String expression4 = " 8 * 8 / ( 5 - 1 ) + 2 * ( 7 - 3 )";

		String expression5 = "6 * ( 9 - 1 ) / ( 6 / ( 1 * 2 ) )";

		String expression6 = "5 + ( ( 2 + ( 1 * 7 ) ) - 9 / ( 0 + 3 ) )";

		String[] expression = new String[] {expression1, expression2, expression3, expression4, expression5, expression6};

		Operation tree;

		System.out.println();

		System.out.println("----- Final Project Test Cases -----");

		System.out.println();

		System.out.println("----- Evaluation ----");

		System.out.println();

		for(int i = 1; i <= 6; i++) {

			System.out.printf("Case %d : %s", i, expression[i-1]);

			System.out.println();

			String postfix = intoPos(expression[i-1]);

			tree = makeExpTreeOOP(postfix);

			System.out.println();

			printTree(tree, 0);

			Visitor v = new Evaluator();

			System.out.println("Evaluation = " + tree.accept(v));

			System.out.println();
			
			System.out.println();

		}

		System.out.println("----- Differentiation & Simplification ----");

		System.out.println();

		expression1 = "(6 * x * x) + (4 * x) - 1";		// 6x^2 + 4x - 1

		expression2 = "(x * x * x * x) - (4 * x * x * x)";		// x^4 - 4x^3 

		expression3 = "((4 * x) - 2) / ((x * x) + 1)";		// (4x - 2) / (x^2 + 1)

		expression4 = "((2 * x) - (4 * x * x * x)) / x"; 	// (2x - 4x^3) / x

		expression5 = "(5 * x) + 5";	// 5x + 5

		expression6 = "(x * x * x) - (4 * x * x) + (6 * x) + 8";	// x^3 - 4x^2 + 6x + 8

		expression = new String[] {expression1, expression2, expression3, expression4, expression5, expression6};

		for(int i = 1; i <= 6; i++) {

			System.out.printf("Case %d : %s\n", i, expression[i-1]);

			System.out.println();

			String postfix = intoPos(expression[i-1]);
			
			tree = makeExpTreeOOP(postfix);
			
			Visitor v = new Diff();
			
			Operation testDiff = (Operation) tree.accept(v);
			
			System.out.print("Differentiation : ");
			
			printInfixOOP(testDiff);
			
			System.out.println();
			
			System.out.println();
			
			Visitor w = new Simplify();
			
			Operation simplify = (Operation) testDiff.accept(w);
			
			System.out.print("Simplification : ");

			printInfixOOP(simplify);
			
			System.out.println();
			
			System.out.println();
			
			System.out.println();
			
		}
	}
}
