
public class Variable extends Operation {
	
	private char datum;
	
	public Variable(char datum) {
		
		super(null, null);
		
		this.datum = datum;
		
	}
	
	public char getDatum() {
		
		return datum;
		
	}

	@Override
	public int eval() {
		
		return datum;
		
	}
	
	public void setLeft(Operation newLeft) {
		
	}

	public void setRight(Operation newRight) {

	}
	
	@Override
	public Operation getLeft() {
		
		return null;
	}

	@Override
	public Operation getRight() {
		
		return null;
		
	}
	
	@Override
	public Object accept(Visitor v) {
		
		return v.visit(this);
	}
	
	public boolean isLeaf() {

		return true;
	}
}