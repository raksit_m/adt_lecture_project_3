
public interface Visitor {
	
	public Object visit(PlusOperation n);
	
	public Object visit(MinusOperation n);
	
	public Object visit(MultiplyOperation n);
	
	public Object visit(DivideOperation n);
	
	public Object visit(Variable n);
	
	public Object visit(Number n);
	
}
