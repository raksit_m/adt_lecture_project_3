
public class OperationVisitor {

	public int visit(Operation e) {

		if(e instanceof PlusOperation) {

			PlusOperation t = (PlusOperation) e;

			return visit(t.getLeft()) + visit(t.getRight());
		}

		else if(e instanceof MinusOperation) {

			MinusOperation t = (MinusOperation) e;

			return visit(t.getLeft()) - visit(t.getRight());
		}

		else if(e instanceof MultiplyOperation) {

			MultiplyOperation t = (MultiplyOperation) e;

			return visit(t.getLeft()) * visit(t.getRight());
		}

		else if(e instanceof DivideOperation) {

			DivideOperation t = (DivideOperation) e;

			return visit(t.getLeft()) / visit(t.getRight());
		}
		
		else {
			
			Number t = (Number) e;
			
			return t.getDatum();
		}
	}
}
