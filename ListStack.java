
public class ListStack<T> implements Stack<T> {
	
	private Node<T> head = null;

	@Override
	public void push(T x) {
		
		head = new Node<T>(x, head);
		
	}
	
	@Override
	public T pop() {
		
		Node<T> temp = head;
		
		head = head.getNext();
		
		return (T) temp.getDatum();
	}

	@Override
	public T peek() {
		
		return (T) head.getDatum();
	}

	@Override
	public boolean isEmpty() {
		
		return head == null;
	}

	@Override
	public void clear() {
		
		head = null;
		
	}

}
