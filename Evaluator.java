
public class Evaluator implements Visitor {
	
	public Object visit(PlusOperation n) {
	
		return (Integer)(n.getLeft().accept(this)) + (Integer)(n.getRight().accept(this));
		
	}

	public Object visit(MinusOperation n) {
		
		return (Integer)(n.getLeft().accept(this)) - (Integer)(n.getRight().accept(this));
		
	}

	public Object visit(MultiplyOperation n) {
		
		return (Integer)(n.getLeft().accept(this)) * (Integer)(n.getRight().accept(this));
		
	}

	public Object visit(DivideOperation n) {
		
		return (Integer)(n.getLeft().accept(this)) / (Integer)(n.getRight().accept(this));
		
	}
	
	public Object visit(Variable n) {
		
		return null;
	}

	public Object visit(Number n) {
		
		return (int)n.getDatum() - '0';
		
	}
}
