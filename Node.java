
public class Node<T> {

	private T datum;

	private Node<T> next;

	public Node(T datum, Node<T> next) {

		this.datum = datum;

		this.next = next;

	}
	
	public T getDatum() {
		
		return datum;
	}
	
	public void setDatum(T newDatum) {
		
		datum = newDatum;
		
	}
	
	public Node<T> getNext() {
		
		return next;
		
	}
	
	public void setNext(Node<T> nextNode) {
		
		next = nextNode;
		
	}
}
