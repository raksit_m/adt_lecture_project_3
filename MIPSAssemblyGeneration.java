
public class MIPSAssemblyGeneration implements Visitor {

	@Override
	public Object visit(PlusOperation n) {

		n.getLeft().accept(this);

		System.out.println("sw $a0 0($sp)");

		System.out.println("addiu $sp $sp -4");

		n.getRight().accept(this);

		System.out.println("lw $t1 4($sp)");

		System.out.println("add $a0 $t1 $a0");

		System.out.println("addiu $sp $sp 4");

		return null;
	}

	@Override
	public Object visit(MinusOperation n) {

		n.getLeft().accept(this);

		System.out.println("sw $a0 0($sp)");

		System.out.println("addiu $sp $sp -4");

		n.getRight().accept(this);

		System.out.println("lw $t1 4($sp)");

		System.out.println("sub $a0 $t1 $a0");

		System.out.println("addiu $sp $sp 4");

		return null;
	}

	@Override
	public Object visit(MultiplyOperation n) {

		n.getLeft().accept(this);

		System.out.println("sw $a0 0($sp)");

		System.out.println("addiu $sp $sp -4");

		n.getRight().accept(this);

		System.out.println("lw $t1 4($sp)");

		System.out.println("mult $t1 $a0");
		
		System.out.println("mfhi $a0");

		System.out.println("addiu $sp $sp 4");

		return null;
	}

	@Override
	public Object visit(DivideOperation n) {
		
		n.getLeft().accept(this);

		System.out.println("sw $a0 0($sp)");

		System.out.println("addiu $sp $sp -4");

		n.getRight().accept(this);

		System.out.println("lw $t1 4($sp)");

		System.out.println("div $t1 $a0");
		
		System.out.println("mflo $a0");

		System.out.println("addiu $sp $sp 4");

		return null;
		
	}

	@Override
	public Object visit(Variable n) {

		System.out.println("li $a0 " + n.getDatum());

		return null;
	}

	@Override
	public Object visit(Number n) {

		System.out.println("li $a0 " + n.getDatum());

		return null;
	}

}
