
public class Simplify implements Visitor {

	@Override
	public Operation visit(PlusOperation n) {

		Operation left = (Operation) (n.getLeft().accept(this));

		Operation right = (Operation) (n.getRight().accept(this));

		if(left.getDatum() == '0') {

			return (Operation) right.accept(this);
		}

		if(right.getDatum() == '0') {

			return (Operation) left.accept(this);
		}

		if(left.isSameExpression(right)) {

			return new MultiplyOperation(new Number('2'), (Operation) left.accept(this));

		}

		return new PlusOperation((Operation)(left.accept(this)), (Operation)(right.accept(this)));
	}

	@Override
	public Operation visit(MinusOperation n) {

		Operation left = (Operation) (n.getLeft().accept(this));

		Operation right = (Operation) (n.getRight().accept(this));

		if(left.isSameExpression(right)) {

			return new Number('0');
		}

		if(left.getDatum() == '0') {

			return (Operation) right.accept(this);
		}

		if(right.getDatum() == '0') {

			return (Operation) left.accept(this);
		}

		return new MinusOperation((Operation)(left.accept(this)), (Operation)(right.accept(this)));
	}

	@Override
	public Operation visit(MultiplyOperation n) {

		Operation left = (Operation) (n.getLeft().accept(this));

		Operation right = (Operation) (n.getRight().accept(this));

		if(left.getDatum() == '0' || right.getDatum() == '0') {

			return new Number('0');
		}

		if(right.getDatum() == '1') {

			return (Operation) (left.accept(this));

		}

		if(left.getDatum() == '1') {

			return (Operation) (right.accept(this));

		}

		return new MultiplyOperation((Operation)(left.accept(this)), (Operation)(right.accept(this)));
	}

	@Override
	public Operation visit(DivideOperation n) {

		Operation left = (Operation) (n.getLeft().accept(this));

		Operation right = (Operation) (n.getRight().accept(this));

		return new DivideOperation((Operation)(left.accept(this)), (Operation)(right.accept(this)));
	}

	@Override
	public Operation visit(Variable n) {

		return n;
	}

	@Override
	public Object visit(Number n) {

		return n;
	}


}
