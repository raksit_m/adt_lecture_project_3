
public abstract class Operation {

	private char datum;

	private Operation left, right;

	public Operation(Operation left, Operation right) {

		this.left = left;

		this.right = right;

	}

	public char getDatum() {

		return datum;

	}
	
	public String toString() {
		
		if(isLeaf()) {
			
			return this.getDatum() + "";
			
		}
		
		return String.format("(%s %s %s)", this.getLeft().toString(), this.getDatum() + "", this.getRight().toString());
		
	}

	public boolean isSameExpression(Operation op) {

		return this.toString().equals(op.toString());
	}

	public abstract int eval();

	public abstract void setLeft(Operation newLeft);

	public abstract void setRight(Operation newRight);

	public abstract Operation getLeft();

	public abstract Operation getRight();

	public abstract boolean isLeaf();

	public abstract Object accept(Visitor v);

}
